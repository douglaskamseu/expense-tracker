import React from "react";
import ChartBar from "./ChartBar";
import "./Chart.css";

const Chart = (props) => {
  //returns a new array of datapoint values
  const dataPointValues = props.dataPoints.map((dataPoint) => dataPoint.value);

  //Spread operator we will pull out all the array values and use it in the max method as standalone elements
  const totalMaximum = Math.max(...dataPointValues); //receives 12 elements and not an array

  return (
    <div className="chart">
      {props.dataPoints.map((dataPoint) => (
        <ChartBar
          key={dataPoint.label}
          value={dataPoint.value}
          maxValue={totalMaximum}
          label={dataPoint.label}
        />
      ))}
    </div>
  );
};

export default Chart;
